//
//  CustomFlowLayout.h
//  RlmDemo
//
//  Created by Nikolai Trandafil on 3/20/15.
//  Copyright (c) 2015 Nikolai Trandafil. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomFlowLayout : UICollectionViewFlowLayout
@property (nonatomic, strong) UIDynamicAnimator *dynamicAnimator;
- (void)resetLayout;

@end
