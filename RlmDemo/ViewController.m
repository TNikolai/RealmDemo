//
//  ViewController.m
//  RlmDemo
//
//  Created by Nikolai Trandafil on 12/27/14.
//  Copyright (c) 2014 Nikolai Trandafil. All rights reserved.
//

#import "ViewController.h"
#import "Note.h"
#import "NoteCollectionViewCell.h"
#import "AddNewNoteViewController.h"
#import "ViewModel.h"
#import "CustomFlowLayout.h"

#import "NoteCollectionViewCellModel.h"


@interface ViewController () <UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout, ViewModelDelegate>

@property (nonatomic)BOOL isDeleteMode;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (strong, nonatomic) ViewModel *model;

@end

@implementation ViewController

- (void)viewDidLayoutSubviews {
    [self.collectionView reloadData];
    [(CustomFlowLayout *)[self.collectionView collectionViewLayout] resetLayout];
    [super viewDidLayoutSubviews];
}


- (ViewModel *)model {
    if(!_model) {
        _model = [[ViewModel alloc] init];
    }
    
    return _model;
}


#pragma mark CollectionView dataSources

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return [self.model numberOfItems];
}


-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    NoteCollectionViewCell * cell = [collectionView dequeueReusableCellWithReuseIdentifier:[NoteCollectionViewCell cellIdentifier] forIndexPath:indexPath];
    if (cell == nil) {
        cell = [[NoteCollectionViewCell alloc]init];
    }
    NoteCollectionViewCellModel *cellModel = [self.model cellModelForIndexPath:indexPath];
    [cell updateInformationWithModel:cellModel];
    
    return cell;  
}


-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    if(self.isDeleteMode == YES) {
        [self.model selectToDeleteItemAtIndexPath:indexPath];
        [collectionView reloadData];
        [(CustomFlowLayout *)self.collectionView.collectionViewLayout resetLayout];
    } else {
        UIViewController *editNoteVc = [self.model viewControllerToPresentAtIndexPath:indexPath];
        [[self navigationController] showViewController:editNoteVc sender:self];
    }
}


#pragma mark Tranzitions and getting new added Note

- (IBAction)addNote:(id)sender {
    UIViewController *addNewNoteVc = [self.model addNoteViewController];
    [[self navigationController] showViewController:addNewNoteVc sender:self];
    self.isDeleteMode = NO;
    [self reloadCollecetionViewOnmainThread];
}


- (IBAction)deleteNote:(id)sender {
   [self.model removeUnchekedNotesFromNotesToDeleteArray];
    self.isDeleteMode = self.isDeleteMode == YES? NO : YES;
    if(self.isDeleteMode == NO) {
        [self deleteSelectedNotes];
    }
}


- (void)deleteSelectedNotes {
    [self.model deleteSelectedNotes];
    [self.collectionView reloadData];
    [(CustomFlowLayout *)[self.collectionView collectionViewLayout] resetLayout];
}


- (void)reloadCollecetionViewOnmainThread {
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        [self.collectionView reloadItemsAtIndexPaths:[self.collectionView indexPathsForVisibleItems]];
    }];
}

@end
