//
//  ViewModel.h
//  RlmDemo
//
//  Created by Nikolai Trandafil on 3/11/15.
//  Copyright (c) 2015 Nikolai Trandafil. All rights reserved.
//

#import <Foundation/Foundation.h>
@class NoteCollectionViewCellModel;
@import UIKit;

@protocol ViewModelDelegate <NSObject>

- (void)reloadCollecetionViewOnmainThread;

@end

@interface ViewModel : NSObject

- (NSInteger)numberOfItems;
- (NoteCollectionViewCellModel *)cellModelForIndexPath:(NSIndexPath *)indexPath;
- (void)selectToDeleteItemAtIndexPath:(NSIndexPath *)indexPath;
- (UIViewController *)viewControllerToPresentAtIndexPath:(NSIndexPath *)indexPath;
- (void)deleteSelectedNotes;
- (void)removeUnchekedNotesFromNotesToDeleteArray;
- (UIViewController *)addNoteViewController;

@end
