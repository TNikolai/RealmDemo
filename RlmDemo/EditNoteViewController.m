//
//  EditNoteViewController.m
//  RlmDemo
//
//  Created by Nikolai Trandafil on 12/29/14.
//  Copyright (c) 2014 Nikolai Trandafil. All rights reserved.
//

#import "EditNoteViewController.h"
#import "Note.h"

@interface EditNoteViewController ()
@property (weak, nonatomic) IBOutlet UITextView *messageTextView;
@property (weak, nonatomic) IBOutlet UIDatePicker *datePicker;
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (nonatomic) UITapGestureRecognizer *tapRecognizer;
@property (weak, nonatomic) IBOutlet UIButton *attachImageButton;

@end

@implementation EditNoteViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(orientationDidChange:) name:UIDeviceOrientationDidChangeNotification object:nil];
    self.tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleSingleTap:)];
    self.tapRecognizer.cancelsTouchesInView = NO;
    [self.view addGestureRecognizer:self.tapRecognizer];
    [self setRightBarButton];
    self.imageView.image = [[UIImage alloc] initWithData:self.editableNotePointer.imageData];
    self.datePicker.date = self.editableNotePointer.date;
    self.messageTextView.text = self.editableNotePointer.text;
}


- (void)handleSingleTap:(UITapGestureRecognizer *) sender {
    [self.messageTextView endEditing:YES];
}

- (void)setRightBarButton {
    if(![self.navigationItem rightBarButtonItem]) {
        UIBarButtonItem *doneItem = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(saveChanges:)];
        self.navigationItem.rightBarButtonItem = doneItem;
    }
}

- (IBAction)saveChanges:(id)sender {
     RLMRealm *realm = [RLMRealm defaultRealm];                                           //Writing Changes in Realm
    [realm beginWriteTransaction];
    self.editableNotePointer.date = self.datePicker.date;
    self.editableNotePointer.text = self.messageTextView.text;
    self.editableNotePointer.imageData = UIImageJPEGRepresentation(self.imageView.image, 0.8);
    [realm commitWriteTransaction];
    [self.navigationController popToRootViewControllerAnimated:YES];
}



- (IBAction)selectPhoto:(UIButton *)sender {
    
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = YES;
    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    
    [self presentViewController:picker animated:YES completion:NULL];
    
    
}


- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    UIImage *chosenImage = info[UIImagePickerControllerEditedImage];
    self.imageView.image = chosenImage;
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
}


- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
}


- (void)orientationDidChange:(NSNotification *)notification {
    if ([UIDevice currentDevice].orientation == UIDeviceOrientationLandscapeLeft || [UIDevice currentDevice].orientation == UIDeviceOrientationLandscapeRight) {
        [self.attachImageButton setHidden:YES];
    } else {
        [self.attachImageButton setHidden:NO];
    }
}

@end
