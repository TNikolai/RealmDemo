//
//  Note.h
//  RlmDemo
//
//  Created by Nikolai Trandafil on 12/27/14.
//  Copyright (c) 2014 Nikolai Trandafil. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Realm/Realm.h>

@interface Note : RLMObject
@property(nonatomic, copy)NSString *text;
@property(nonatomic, copy)NSDate   *date;
@property(nonatomic, strong)NSData *imageData;
@property(nonatomic)BOOL isChangedToDelete;

- (instancetype)initWithDate:(NSDate *)date noteText:(NSString *)text deleteNote:(BOOL)del imageData:(NSData *)imageData;
- (void) editNoteWithDate: (NSDate *)date editMessage: (NSString *)text;

@end
