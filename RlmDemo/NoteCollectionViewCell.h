//
//  NoteCVCellCollectionViewCell.h
//  RlmDemo
//
//  Created by Nikolai Trandafil on 12/27/14.
//  Copyright (c) 2014 Nikolai Trandafil. All rights reserved.
//

#import <UIKit/UIKit.h>
@class NoteCollectionViewCellModel;

@interface NoteCollectionViewCell : UICollectionViewCell
- (void)updateInformationWithModel:(NoteCollectionViewCellModel *)model;
+ (NSString *)cellIdentifier;

@end
