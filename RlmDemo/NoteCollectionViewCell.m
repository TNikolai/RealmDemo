//
//  NoteCVCellCollectionViewCell.m
//  RlmDemo
//
//  Created by Nikolai Trandafil on 12/27/14.
//  Copyright (c) 2014 Nikolai Trandafil. All rights reserved.
//

#import "NoteCollectionViewCell.h"
#import "Note.h"
#import "NoteCollectionViewCellModel.h"

@interface NoteCollectionViewCell ()
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UITextView *textView;
@property (weak, nonatomic) IBOutlet UIImageView *backGroundImageView;
@end

@implementation NoteCollectionViewCell      

- (void)updateInformationWithModel:(NoteCollectionViewCellModel *)noteCvCell {
    self.dateLabel.text = noteCvCell.date;
    self.textView.text = noteCvCell.title;
    self.imageView.hidden = noteCvCell.imageIsHidden;
    if (noteCvCell.imageData) {
        UIImage *image = [UIImage imageWithData:noteCvCell.imageData];
        [self.backGroundImageView setImage:image];
    } else {
            [self.backGroundImageView setImage:[UIImage imageNamed:@"default_75.png"]];
    }
}

+ (NSString *)cellIdentifier {
    return @"NoteCollectionViewCell";
}

@end
