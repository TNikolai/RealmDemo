//
//  NoteCollectionViewCellModel.h
//  RlmDemo
//
//  Created by Nikolai Trandafil on 3/11/15.
//  Copyright (c) 2015 Nikolai Trandafil. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NoteCollectionViewCellModel : NSObject

@property (strong, nonatomic) NSString *title;
@property (assign, nonatomic) BOOL imageIsHidden;
@property (strong, nonatomic) NSString *date;
@property (strong, nonatomic) NSData *imageData;

@end
