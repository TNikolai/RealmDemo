//
//  Note.m
//  RlmDemo
//
//  Created by Nikolai Trandafil on 12/27/14.
//  Copyright (c) 2014 Nikolai Trandafil. All rights reserved.
//

#import "Note.h"

@implementation Note

- (instancetype)initWithDate:(NSDate *)date noteText:(NSString *)text deleteNote:(BOOL)del imageData:(NSData *)imageData {
    self = [super init];
    if(self) {
        self.date = date;
        self.text = text;
        self.imageData = imageData;
        self.isChangedToDelete = del;
    }
    
    return self;
}


- (void) editNoteWithDate:(NSDate *)date editMessage: (NSString *)text {
    self.date = date;
    self.text = text;
}


- (NSData *)imageData {
    _imageData = _imageData? _imageData : UIImageJPEGRepresentation([UIImage imageNamed:@"default_75.png"], 0.8);
    
    return _imageData;
}

@end
