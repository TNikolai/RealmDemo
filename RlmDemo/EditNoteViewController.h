//
//  EditNoteViewController.h
//  RlmDemo
//
//  Created by Nikolai Trandafil on 12/29/14.
//  Copyright (c) 2014 Nikolai Trandafil. All rights reserved.
//

#import "ViewController.h"
@class Note;

@interface EditNoteViewController : ViewController
@property(strong, nonatomic) Note *editableNotePointer;

@end
