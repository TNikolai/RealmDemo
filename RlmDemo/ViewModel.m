//
//  ViewModel.m
//  RlmDemo
//
//  Created by Nikolai Trandafil on 3/11/15.
//  Copyright (c) 2015 Nikolai Trandafil. All rights reserved.
//

#import "ViewModel.h"
#import "NoteCollectionViewCellModel.h"
#import "Note.h"
#import <Realm/Realm.h>
#import "EditNoteViewController.h"
#import "AddNewNoteViewController.h"

RLM_ARRAY_TYPE(Note);


@interface ViewModel () <ReturnNoteToViecontrollerDelegate>

@property(readwrite,nonatomic,copy)RLMArray<Note> *realmNotesArray;                      //RLMArray with type Note
@property (strong,nonatomic)NSMutableArray *notestoDelete;
@end

@implementation ViewModel

- (RLMArray *)realmNotesArray{
    _realmNotesArray = _realmNotesArray ? : (RLMArray<Note> *)[Note allObjects];        //Gets all Note objects from default Realm
    
    return _realmNotesArray;
}


-(NSMutableArray *)notestoDelete {
    _notestoDelete = _notestoDelete? : [[NSMutableArray alloc]init];
    
    return _notestoDelete;
}


- (NSInteger)numberOfItems {
    return [self.realmNotesArray count];
}


- (NoteCollectionViewCellModel *)cellModelForIndexPath:(NSIndexPath *)indexPath {
    Note *note = self.realmNotesArray[indexPath.row];
    NoteCollectionViewCellModel *cellModel = [[NoteCollectionViewCellModel alloc] init];
    cellModel.title = note.text;
    cellModel.imageIsHidden = note.isChangedToDelete == YES? NO:YES;
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"HH:mm                    dd.MM.yyyy"];
    [formatter setTimeZone:[NSTimeZone defaultTimeZone]];
    NSString *stringFromDate = [formatter stringFromDate:note.date];
    cellModel.date = stringFromDate;
    cellModel.imageData = note.imageData;

    return cellModel;
}


- (UIViewController *)viewControllerToPresentAtIndexPath:(NSIndexPath *)indexPath {
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    EditNoteViewController *editNoteVc = [mainStoryboard instantiateViewControllerWithIdentifier:@"EditNoteViewController"];
    editNoteVc.editableNotePointer = self.realmNotesArray[indexPath.row];
    return editNoteVc;
    
}


#pragma mark Removing Note

- (void)deleteSelectedNotes {
    if ([self.notestoDelete count] == 0) {
        return;
    }
    RLMRealm *realm = [RLMRealm defaultRealm];                //Using Block to delete Array of Notes From Realm
    [realm transactionWithBlock: ^{
        [realm deleteObjects:self.notestoDelete];
        [self.notestoDelete removeAllObjects];
    }];
}


- (void)removeUnchekedNotesFromNotesToDeleteArray {
    NSMutableArray *notes = [[NSMutableArray alloc] init];
    for(Note *note in self.notestoDelete) {
        if(note.isChangedToDelete) {
            [notes addObject:note];
        }
    }
    self.notestoDelete = notes;
}


+ (NSString *) applicationDocumentsDirectory {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *basePath = ([paths count] > 0) ? [paths objectAtIndex:0] : nil;
    return basePath;
}


- (void)selectToDeleteItemAtIndexPath:(NSIndexPath *)indexPath {
    RLMRealm *realm = [RLMRealm defaultRealm];                      //Modify Note attributes in Realm
    [realm beginWriteTransaction];
    Note *changedNote = self.realmNotesArray[indexPath.row];
    changedNote.isChangedToDelete = !changedNote.isChangedToDelete;
    
    if(changedNote.isChangedToDelete) {
        [self.notestoDelete addObject:changedNote];
    } else {
        [self.notestoDelete removeObject:changedNote];
    }
    [realm commitWriteTransaction];
}


#pragma mark Adding new Note

- (UIViewController *)addNoteViewController {
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    AddNewNoteViewController *addNewNoteVc = [mainStoryboard instantiateViewControllerWithIdentifier:@"AddNewNoteViewController"];
    addNewNoteVc.delegate = self;
    
    return addNewNoteVc;
}


- (void)getNewNote:(Note *)note {
    note.date = note.date ? : [NSDate date];
    RLMRealm *realm = [RLMRealm defaultRealm];          //After receiving new Note adding her in Realm
    [realm transactionWithBlock: ^{
        [realm addObject:note];
    }];

}

@end
