//
//  AddNewNoteViewController.m
//  RlmDemo
//
//  Created by Nikolai Trandafil on 12/27/14.
//  Copyright (c) 2014 Nikolai Trandafil. All rights reserved.
//

#import "AddNewNoteViewController.h"
#import "Note.h"

@interface AddNewNoteViewController () <UIImagePickerControllerDelegate>
@property (weak, nonatomic) IBOutlet UITextView  *noteText;
@property (weak, nonatomic) IBOutlet UIDatePicker *datePicker;
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (nonatomic) UITapGestureRecognizer *tapRecognizer;
@property (weak, nonatomic) IBOutlet UIButton *attachImageButton;

@end

@implementation AddNewNoteViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(orientationDidChange:) name:UIDeviceOrientationDidChangeNotification object:nil];
    self.tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleSingleTap:)];
    self.tapRecognizer.cancelsTouchesInView = NO;
    [self.view addGestureRecognizer:self.tapRecognizer];
    [self setRightBarButton];
    [self.imageView setImage:[UIImage imageNamed:@"default_75.png"]];
}


- (void)handleSingleTap:(UITapGestureRecognizer *) sender {
    [self.noteText endEditing:YES];
}


- (void)setRightBarButton {
    if(![self.navigationItem rightBarButtonItem]) {
        UIBarButtonItem *doneItem = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(saveNote:)];
        self.navigationItem.rightBarButtonItem = doneItem;
    }
}


- (IBAction)saveNote:(id)sender {
    NSData *imageData = UIImageJPEGRepresentation(self.imageView.image, 0.8);
    Note *newNote = [[Note alloc]initWithDate:self.datePicker.date noteText:self.noteText.text deleteNote:NO imageData:imageData];
    [self.delegate getNewNote:newNote];
    [self.navigationController popViewControllerAnimated:YES];

}


- (IBAction)selectPhoto:(UIButton *)sender {
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = YES;
    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    [self presentViewController:picker animated:YES completion:NULL];
}


- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    UIImage *chosenImage = info[UIImagePickerControllerEditedImage];
    self.imageView.image = chosenImage;
    [picker dismissViewControllerAnimated:YES completion:NULL];
}


- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [picker dismissViewControllerAnimated:YES completion:NULL];
}


- (void)orientationDidChange:(NSNotification *)notification {
    if ([UIDevice currentDevice].orientation == UIDeviceOrientationLandscapeLeft || [UIDevice currentDevice].orientation == UIDeviceOrientationLandscapeRight) {
        [self.attachImageButton setHidden:YES];
    } else {
        [self.attachImageButton setHidden:NO];
    }
}

@end
