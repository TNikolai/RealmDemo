//
//  AddNewNoteViewController.h
//  RlmDemo
//
//  Created by Nikolai Trandafil on 12/27/14.
//  Copyright (c) 2014 Nikolai Trandafil. All rights reserved.
//

#import "ViewController.h"
@class Note;

@protocol ReturnNoteToViecontrollerDelegate <NSObject>
- (void)getNewNote: (Note *)note;

@end


@interface AddNewNoteViewController : ViewController
@property(nonatomic, weak) id<ReturnNoteToViecontrollerDelegate>delegate;
@end
